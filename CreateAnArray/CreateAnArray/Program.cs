﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreateAnArray
{
    class Program
    {
        static void Main(string[] args)
        {
            var colour = new string[5] { "red", "Blue", "Orange", "white", "black" };
            for (var i = 0; i < colour.Length; i++)
            {
                Console.WriteLine(colour[i]);
            }
        }
    }
}
